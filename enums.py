from enum import Enum

class Action(Enum):
  DO_NOTHING = 0
  MOVE_FORWARD = 1
  TURN_RIGHT = 2
  TURN_LEFT = 3

class Direction(Enum):
  NORTH = 0
  EAST = 1
  SOUTH = 2
  WEST = 3
