from enums import *

class Robot:

  def __init__(self, number, position, orientation, destination, boardSize):
    self.number = number
    self.position = position
    self.orientation = orientation
    self.destination = destination
    self.boardSize = boardSize;

  def update(self, robots):
    # Logica para o eixo X
    distX = self.position.x - self.destination.x   
    if distX > 0:
      if self.orientation == Direction.NORTH:
        return Action.TURN_LEFT
      elif self.orientation == Direction.SOUTH:
        return Action.TURN_RIGHT
      elif self.orientation == Direction.EAST:
        return Action.TURN_RIGHT
      else:
        return Action.MOVE_FORWARD  
    if distX < 0:
      if self.orientation == Direction.NORTH:
        return Action.TURN_RIGHT
      elif self.orientation == Direction.SOUTH:
        return Action.TURN_LEFT
      elif self.orientation == Direction.WEST:
        return Action.TURN_RIGHT
      else:
        return Action.MOVE_FORWARD
      return Action.DO_NOTHING

    # Lógica para o eixo Y
    distY = self.position.y - self.destination.y
    if distY > 0:
      if self.orientation == Direction.WEST:
        return Action.TURN_RIGHT
      elif self.orientation == Direction.SOUTH:
        return Action.MOVE_RIGHT
      elif self.orientation == Direction.EAST:
        return Action.TURN_LEFT
      else:
        return Action.MOVE_FORWARD
    if distY < 0:
      if self.orientation == Direction.EAST:
        return Action.TURN_RIGHT
      elif self.orientation == Direction.NORTH:
        return Action.TURN_RIGHT
      elif self.orientation == Direction.WEST:
        return Action.TURN_LEFT
      return Action.MOVE_FORWARD
    return Action.DO_NOTHING

    # Evitar bater no fim da tabela
    #if Direction.EAST and Action.MOVE_FORWARD and 



      
