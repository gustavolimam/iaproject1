import sys, pygame
from robot import *
from random import randint
from point import Point
from pygame import gfxdraw
from robot_info import *

screen_size = width, height = 640, 480
black = 0, 0, 0
white = 255, 255, 255
blue = 0, 0, 255
green = 0, 255, 0
red = 255, 0, 0
yellow = 255, 255, 0
gray = 200, 200, 200

class Environment:
  def __init__(self, size, robot_qty):
    self.rectSize = 40
    self.size = size
    self.robot_qty = robot_qty
    self.qty_on_destination = 0
    self.timestamp = 0
    self.crash = False
    self.board = []
    for i in range(size):
      self.board.append([])
      for j in range(size):
        self.board[i].append(0)
    self.createRobots()
    self.startX = width/2-self.rectSize*size/2
    self.startY = height/2-self.rectSize*size/2

  def createRobots(self):
    self.robots = []
    for i in range(self.robot_qty):
      self.createRobot(i)

  def createRobot(self, number):
    destX = randint(0, self.size - 1)
    destY = randint(0, self.size - 1)
    while self.board[destY][destX]!=0:
      destX = randint(0, self.size - 1)
      destY = randint(0, self.size - 1)
    self.board[destY][destX] += 1
    destination = Point(destX, destY)
    posX = randint(0, self.size - 1)
    posY = randint(0, self.size - 1)
    while self.board[posY][posX]!=0:
      posX = randint(0, self.size - 1)
      posY = randint(0, self.size - 1)
    self.board[posY][posX] += 1
    position = Point(posX, posY)
    orientation = Direction(randint(0, 3))
    self.robots.append(Robot(number, position, orientation, destination, self.size))
    
  def draw(self):
    screen.fill(white)
    self.drawEnvironment()
    self.drawRobots()
    self.drawTimestamp()
    if self.crash:
      label = crashFont.render("Crash", True, red)
      screen.blit(label, (width/2-200, height/2-90))
    pygame.display.flip()

  def drawEnvironment(self):
    for i in range(self.size):
      for j in range(self.size):
        r = (i*self.rectSize+self.startX, j*self.rectSize+self.startY, self.rectSize, self.rectSize)
        pygame.draw.rect(screen, gray, r)
        pygame.draw.rect(screen, black, r, 1)

  def drawRobots(self):
    for robot in self.robots:
      self.drawRobot(robot)

  def drawTimestamp(self):
    label = textfont.render("Timestamp: "+str(self.timestamp), True, black)
    screen.blit(label, (5, 0))

  def drawRobot(self, robot):
    r = ((robot.position.x+1)*self.rectSize+2,(robot.position.y+1)*self.rectSize+2, self.rectSize-4, self.rectSize-4)
    if robot.orientation==Direction.NORTH:
      p1 = ((robot.position.x+0.5)*self.rectSize+self.startX, (robot.position.y)*self.rectSize+2+self.startY)
      p2 = ((robot.position.x)*self.rectSize+self.startX+2, (robot.position.y+1)*self.rectSize-4+self.startY)
      p3 = ((robot.position.x+1)*self.rectSize-4+self.startX, (robot.position.y+1)*self.rectSize-4+self.startY)
    elif robot.orientation==Direction.EAST:
      p1 = (self.startX+(robot.position.x+1)*self.rectSize-4, self.startY+(robot.position.y+0.5)*self.rectSize)
      p2 = (self.startX+(robot.position.x)*self.rectSize+2, self.startY+(robot.position.y)*self.rectSize+2)
      p3 = (self.startX+(robot.position.x)*self.rectSize+2, self.startY+(robot.position.y+1)*self.rectSize-4)
    elif robot.orientation==Direction.SOUTH:
      p1 = (self.startX+(robot.position.x+0.5)*self.rectSize, self.startY+(robot.position.y+1)*self.rectSize-4)
      p2 = (self.startX+(robot.position.x)*self.rectSize+2, self.startY+(robot.position.y)*self.rectSize+2)
      p3 = (self.startX+(robot.position.x+1)*self.rectSize-4, self.startY+(robot.position.y)*self.rectSize+2)
    elif robot.orientation==Direction.WEST:
      p1 = (self.startX+(robot.position.x)*self.rectSize+2, self.startY+(robot.position.y+0.5)*self.rectSize)
      p2 = (self.startX+(robot.position.x+1)*self.rectSize-4, self.startY+(robot.position.y)*self.rectSize+2)
      p3 = (self.startX+(robot.position.x+1)*self.rectSize-4, self.startY+(robot.position.y+1)*self.rectSize-4)
    t = (p1, p2, p3)
    gfxdraw.filled_polygon(screen, t, white)
    gfxdraw.aapolygon(screen, t, black)
    label = textfont.render(str(robot.number), 1, black)
    screen.blit(label, (self.startX+robot.position.x*self.rectSize+self.rectSize/3,self.startY+robot.position.y*self.rectSize))
    label = textfont.render(str(robot.number), 1, black)
    screen.blit(label, (self.startX+robot.destination.x*self.rectSize+self.rectSize/3,self.startY+robot.destination.y*self.rectSize))
    
  def turnRight(self, robot):
    robot.orientation = Direction((robot.orientation.value+1)%4)

  def turnLeft(self, robot):
    robot.orientation = Direction((robot.orientation.value+3)%4)
        
  def moveForward(self, robot):
    if robot.orientation == Direction.NORTH:
      robot.position.y -= 1
    elif robot.orientation == Direction.EAST:
      robot.position.x += 1
    elif robot.orientation == Direction.SOUTH:
      robot.position.y += 1
    elif robot.orientation == Direction.WEST:
      robot.position.x -= 1
    if robot.position.x < 0:
      robot.position.x = 0
    elif robot.position.x >= self.size:
      robot.position.x = self.size - 1
    elif robot.position.y < 0:
      robot.position.y = 0
    elif robot.position.y >= self.size:
      robot.position.y = self.size - 1

  def copy(self, r1, r2):
    r2.number = r1.number
    r2.position.x = r1.position.x
    r2.position.y = r1.position.y
    r2.orientation = r1.orientation
    r2.destination.x = r1.destination.x
    r2.destination.y = r1.destination.y
    r2.boardSize = r1.boardSize;

  def update(self):
    if self.isOver():
      return
    self.qty_on_destination = 0
    self.timestamp += 1
    robots_info = []
    for robot in self.robots:
      robots_info.append(RobotInfo(robot.number, Point(robot.position.x, robot.position.y), robot.orientation, Point(robot.destination.x, robot.destination.y), robot.boardSize))
    for i in range(self.size):
      for j in range(self.size):
        self.board[i][j] = 0
    for robot in self.robots:
      r = Robot(robot.number, Point(robot.position.x, robot.position.y), robot.orientation, Point(robot.destination.x, robot.destination.y), robot.boardSize)
      movement = robot.update(robots_info)
      self.copy(r, robot)
      if movement == Action.MOVE_FORWARD:
        self.moveForward(robot)
      elif movement == Action.TURN_RIGHT:
        self.turnRight(robot)
      elif movement == Action.TURN_LEFT:
        self.turnLeft(robot)
      if robot.position.x == robot.destination.x and robot.position.y == robot.destination.y:
        self.qty_on_destination += 1
      self.board[robot.position.x][robot.position.y]+=1
    for i in range(self.size):
      for j in range(self.size):
        if self.board[i][j] > 1:
          self.crash = True
        
  def isOver(self):
    return self.qty_on_destination == self.robot_qty or self.crash
      
env = Environment(10, 5)
pygame.init()
screen = pygame.display.set_mode(screen_size)
textfont = pygame.font.SysFont("Arial", 30)
crashFont = pygame.font.SysFont("Arial", 150)
crashFont.set_bold(True)
while True:
  pygame.time.wait(1000)
  for event in pygame.event.get():
    if event.type == pygame.QUIT:
      sys.exit()
  if env.isOver():
    sys.exit()
  env.update()
  env.draw()
