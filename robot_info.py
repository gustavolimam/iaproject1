class RobotInfo:
  def __init__(self, number, position, orientation, destination, boardSize):
    self.number = number
    self.position = position
    self.orientation = orientation
    self.destination = destination
    self.boardSize = boardSize
